import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../../providers/geocoder/geocoder';
import { TranslateService } from '@ngx-translate/core'
import { PagerProvider } from '../../../providers/pager/pager';

@IonicPage()
@Component({
  selector: 'page-fuel-consumption-report',
  templateUrl: 'fuel-consumption-report.html',
})
export class FuelConsumptionReportPage {
  islogin: any;
  portstemp: any[] = [];
  datetimeStart: any;
  datetimeEnd: any;
  vehId: any;
  _listData: any[] = [];
  fuelDataArr: any[] = [];
  eventT: any;
  reportType: any[] = [];
  repkey: string = this.translate.instant('Time');
  selectedVehicle: any;

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider,
    public translate: TranslateService,
    private modalCtrl: ModalController,
    private pagerService: PagerProvider) {
    // this.reportType = ["Time", "Daily", "Trip"];
    this.reportType = [this.translate.instant('Time'), this.translate.instant('Daily'), this.translate.instant('Trip')];
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuelConsumptionReportPage');
  }
  ngOnInit() {
    this.getdevices();
  }

  getFuelNotifs(fuelData, key) {
    this.getData(fuelData, key);

  }

  getData(fuelData, key) {
    // this.eventType = key;
    // const paramData = this.navParams.get('paramMaps');
    const _url = this.apiCall.mainUrl + "gps/getFuelNotifs";
    const payload = {
      "start_time": fuelData.start_time,
      "end_time": fuelData.end_time,
      "imei": fuelData.imei,
      "event": key
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(_url, payload)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        console.log("response: ", resp)
        // this.fuelData = resp;
        this.modalCtrl.create('FuelEventsComponent', {
          paramMaps: resp,
          // event: key
        }).present();
      },
        err => {
          this.apiCall.stopLoading();
        })
  }

  reportWise(repId) {
    console.log("repId: ", repId);
  }

  getId(veh) {
    this.vehId = veh;
    // this.vehId = veh.Device_ID;
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getReport() {
    var payload = {};
    var _basU = this.apiCall.mainUrl + "gps/updatedFuelReport";
    if (this.repkey === this.translate.instant('Daily') || this.repkey === this.translate.instant('Trip')) {
      if (this.vehId == undefined) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Please Select Vehicle..'),
          duration: 1500,
          position: "bottom"
        })
        toast.present();
      } else {
        if (this.repkey === this.translate.instant('Daily')) {
          payload = {
            "from": new Date(this.datetimeStart).toISOString(),
            "to": new Date(this.datetimeEnd).toISOString(),
            "user": this.islogin._id,
            "daywise": true,
            "imei": this.vehId.Device_ID,
            "vehicle": this.vehId._id
          }
        } else if (this.repkey === this.translate.instant('Trip')) {
          payload = {
            "from": new Date(this.datetimeStart).toISOString(),
            "to": new Date(this.datetimeEnd).toISOString(),
            "user": this.islogin._id,
            "trip": true,
            "imei": this.vehId.Device_ID,
            "vehicle": this.vehId._id
          }
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_basU, payload)
          .subscribe(res => {
            this.apiCall.stopLoading();
            // console.log("report data: ", res)
            this.fuelDataArr = [];

            if (res.message === undefined) {
              this.fuelDataArr = res;
              // initialize to page 1
              // this.setPage(1);
            } else {
              let toast = this.toastCtrl.create({
                message: "No data found..",
                duration: 1500,
                position: 'bottom'
              });
              toast.present();
            }
          },
            err => {
              this.apiCall.stopLoading();
            });
      }
    } else if (this.repkey === this.translate.instant('Time')) {
      if (this.vehId != undefined) {
        payload = {
          "from": new Date(this.datetimeStart).toISOString(),
          "to": new Date(this.datetimeEnd).toISOString(),
          "imei": this.vehId.Device_ID,
          "vehicle": this.vehId._id
        }
      } else {
        payload = {
          "from": new Date(this.datetimeStart).toISOString(),
          "to": new Date(this.datetimeEnd).toISOString(),
          "user": this.islogin._id
        }
      }
      this.apiCall.startLoading().present();
      this.apiCall.urlpasseswithdata(_basU, payload)
        .subscribe(res => {
          this.apiCall.stopLoading();
          console.log("report data: ", res)
          this.fuelDataArr = [];
          if (res.message === undefined) {
            this.fuelDataArr = res;
            // initialize to page 1
            this.setPage(1);
          } else {
            let toast = this.toastCtrl.create({
              message: "No data found..",
              duration: 1500,
              position: 'bottom'
            });
            toast.present();
          }
        });
    }

  }

  setPage(page: number) {
    debugger
    // get pager object from service
    this.pager = this.pagerService.getPager(this.fuelDataArr.length, page);
    if (page >= 2) {
      var someArr = this.fuelDataArr.slice(this.pager.startIndex, this.pager.endIndex + 1);
      for (var i = 0; i < someArr.length; i++) {
        this.pagedItems.push(someArr[i]);
      }
    } else {
      // get current page of items
      this.pagedItems = this.fuelDataArr.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    console.log("pagedItems: ", this.pagedItems)
  }

  start_address(item, index) {
    let that = this;
    that.fuelDataArr[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.fuelDataArr[index].StartLocation = "N/A";
    } else if (item.start_location) {
      this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
        .then((res) => {
          console.log("test", res)
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          that.fuelDataArr[index].StartLocation = str;
        })
    }
  }

  end_address(item, index) {
    let that = this;
    that.fuelDataArr[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.fuelDataArr[index].EndLocation = "N/A";
    } else if (item.end_location) {
      this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
        .then((res) => {
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
          that.fuelDataArr[index].EndLocation = str;
        })
    }
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }
  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
    return hDisplay + mDisplay + sDisplay;
  }

  // doInfinite(infiniteScroll) {
  //   // let that = this;
  //   // that.page = that.page + 1;
  //   setTimeout(() => {
  //     debugger
  //     this.setPage(this.pager.currentPage + 1)
  //     infiniteScroll.complete();
  //   }, 500);
  // }
}
