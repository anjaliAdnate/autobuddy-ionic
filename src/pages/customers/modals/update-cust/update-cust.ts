import { Component, OnInit } from "@angular/core";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";
import { ViewController, NavParams, ToastController, AlertController, IonicPage } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as moment from 'moment';
import { TranslateService } from "@ngx-translate/core";

@IonicPage()
@Component({
    selector: 'page-update-cust',
    templateUrl: './update-cust.html'
})

export class UpdateCustModalPage implements OnInit {
    getAllDealersData: any;
    dealerdata: any;
    isSuperAdminStatus: boolean;
    updatecustForm: FormGroup;
    islogin: any;
    submitAttempt: boolean;
    devicedetail: any = {};
    customer: any;
    vehType: any;
    editdata: any;
    yearLater: any;
    minDate: any;
    customerdata: any = {};

    constructor(
        public apiCall: ApiServiceProvider,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        navPar: NavParams,
        public toastCtrl: ToastController,
        public alerCtrl: AlertController,
        private translate: TranslateService
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        this.customer = navPar.get("param");

        if (this.customer.expiration_date == null) {
            var tru = moment(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var tempdate = new Date(tru);
            tempdate.setDate(tempdate.getDate() + 365);
            this.yearLater = moment(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
        } else {
            this.yearLater = moment(new Date(this.customer.expiration_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
        }

        // ============== one month later date from current date ================
        var tdate = new Date();
        var eightMonthsFromJan312009 = tdate.setMonth(tdate.getMonth() + 1);
        this.minDate = moment(new Date(eightMonthsFromJan312009), 'DD-MM-YYYY').format("YYYY-MM-DD");
        // =============== end
        this.updatecustForm = formBuilder.group({
            userid: [this.customer.userid, Validators.required],
            first_name: [this.customer.first_name, Validators.required],
            last_name: [this.customer.last_name, Validators.required],
            email: [this.customer.email, Validators.required],
            phone: [this.customer.phone, Validators.required],
            address: [this.customer.address, Validators.required],
            creationdate: [moment(new Date(this.customer.created_on), 'DD/MM/YYYY').format('YYYY-MM-DD')],
            expirationdate: [this.yearLater],
            dealer_firstname: [this.customer.dealer_firstname],
        })

        // this.updatecustForm.patchValue()
    }

    ngOnInit() {
        this.getAllDealers();
    }

    // getAllDealers() {
    //     var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles';
    //     this.apiCall.getAllDealerVehiclesCall(baseURLp)
    //         .subscribe(data => {
    //             this.getAllDealersData = data;
    //         }, err => {
    //             console.log(err);
    //         });
    // }

    getAllDealers() {
        var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        let toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.apiCall.getAllDealerCall(baseURLp)
            .subscribe(data => {
                this.getAllDealersData = data;
                // toast.dismiss();
            },
                error => {
                    console.log(error)
                });
    }

    DealerselectData(dealerselect) {
        this.dealerdata = dealerselect;
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    // addcustomer() {
    //     this.submitAttempt = true;
    //     // console.log(devicedetails);
    //     if (this.updatecustForm.valid) {
    //         debugger;
    //         if (this.islogin.isSuperAdmin == true) {
    //             this.customerdata = {
    //                 "first_name": this.updatecustForm.value.first_name,
    //                 "last_name": this.updatecustForm.value.last_name,
    //                 "email": this.updatecustForm.value.email,
    //                 "phone": this.updatecustForm.value.contact_num,
    //                 "password": this.updatecustForm.value.password,
    //                 "isDealer": false,
    //                 "custumer": true,
    //                 "status": true,
    //                 "user_id": this.updatecustForm.value.userid,
    //                 "address": this.updatecustForm.value.address,
    //                 "supAdmin": this.islogin._id
    //             }
    //         } else {
    //             if (this.islogin.isDealer == true) {
    //                 this.customerdata = {
    //                     "first_name": this.updatecustForm.value.first_name,
    //                     "last_name": this.updatecustForm.value.last_name,
    //                     "email": this.updatecustForm.value.email,
    //                     "phone": this.updatecustForm.value.contact_num,
    //                     "password": this.updatecustForm.value.password,
    //                     "isDealer": this.islogin.isDealer,
    //                     "custumer": true,
    //                     "status": true,
    //                     "user_id": this.updatecustForm.value.userid,
    //                     "address": this.updatecustForm.value.address,
    //                     "supAdmin": this.islogin.supAdmin,
    //                     // "Dealer": this.islogin.Dealer_ID['_id']
    //                 }
    //             }
    //         }

    //         if (this.dealerdata != undefined) {
    //             this.customerdata.Dealer = this.dealerdata.dealer_id;
    //         } else {
    //             this.customerdata.Dealer = this.islogin._id;
    //         }

    //         this.apiCall.startLoading().present();
    //         this.apiCall.signupApi(this.customerdata)
    //             .subscribe(data => {
    //                 this.apiCall.stopLoading();
    //                 // this.Customeradd = data;
    //                 let toast = this.toastCtrl.create({
    //                     message: this.translate.instant('dealeradded', { value: this.translate.instant('cust') }),
    //                     position: 'top',
    //                     duration: 1500
    //                 });

    //                 toast.onDidDismiss(() => {
    //                     this.viewCtrl.dismiss();
    //                 });

    //                 toast.present();
    //             },
    //                 err => {
    //                     this.apiCall.stopLoading();
    //                     var body = err._body;
    //                     var msg = JSON.parse(body);
    //                     var namepass = [];
    //                     namepass = msg.split(":");
    //                     var name = namepass[1];
    //                     let alert = this.alerCtrl.create({
    //                         message: name,
    //                         buttons: [this.translate.instant('Okay')]
    //                     });
    //                     alert.present();
    //                 });
    //     }
    // }

    updateCustomer() {
        this.submitAttempt = true;
        if (this.updatecustForm.valid) {
            this.devicedetail = {
                "contactid": this.customer._id,
                "address": this.updatecustForm.value.address,
                "expire_date": new Date(this.updatecustForm.value.expirationdate).toISOString(),
                "first_name": this.updatecustForm.value.first_name,
                "last_name": this.updatecustForm.value.last_name,
                "status": this.customer.status,
                "user_id": this.updatecustForm.value.userid,
                "email": this.updatecustForm.value.email
            }
            if (this.dealerdata != undefined) {
                this.devicedetail.Dealer = this.dealerdata.dealer_id;
            } 
            // else {
            //     this.devicedetail.Dealer = this.islogin._id;
            // }

            if (this.vehType == undefined) {
                this.devicedetail;
            } else {
                this.devicedetail.vehicleType = this.vehType._id;
            }

            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(this.devicedetail)
                .subscribe(data => {
                    this.apiCall.stopLoading();
                    this.editdata = data;
                    let toast = this.toastCtrl.create({
                        message: this.translate.instant('dealerUpdated', { value: this.translate.instant('cust') }),
                        position: 'bottom',
                        duration: 2000
                    });

                    toast.onDidDismiss(() => {
                        this.viewCtrl.dismiss(this.editdata);
                    });

                    toast.present();
                }, err => {
                    this.apiCall.stopLoading();
                    var body = err._body;
                    var msg = JSON.parse(body);
                    let alert = this.alerCtrl.create({
                        message: msg.message,
                        buttons: [this.translate.instant('Okay')]
                    });
                    alert.present();
                });
        }

    }
}
