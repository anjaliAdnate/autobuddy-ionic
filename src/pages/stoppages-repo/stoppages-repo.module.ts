import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoppagesRepoPage } from './stoppages-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    StoppagesRepoPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(StoppagesRepoPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ],
})
export class StoppagesRepoPageModule {}
