import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-trip-report',
  templateUrl: 'trip-report.html',
})
export class TripReportPage implements OnInit {
  islogin: any;
  devices: any;
  portstemp: any = [];
  isdevice: string;
  device_id: any;
  isdeviceTripreport: string;
  datetimeStart: string;
  datetimeEnd: string;
  TripReportData: any[];
  TripsdataAddress: any[];
  deviceId: any;
  distanceBt: number;
  StartTime: string;
  Startetime: string;
  Startdate: string;
  EndTime: string;
  Endtime: string;
  Enddate: string;
  datetime: number;
  did: any;
  vehicleData: any;
  locationEndAddress: any;
  locationAddress: any;
  allData: any = {};
  mapData: any[];
  selectedVehicle: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    if (navParams.get('param') != null) {
      this.vehicleData = navParams.get('param');
    }
  }

  ngOnInit() {
    if (this.vehicleData == undefined) {
      this.getdevices();
    } else {
      this.device_id = this.vehicleData._id;
      this.getTripReport()
    }
  }

  getdevices() {
    var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalligi.startLoading().present();
    this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
        this.isdevice = localStorage.getItem('devices1243');
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  getTripdevice(item) {
    this.device_id = item._id;
    console.log("device id=> " + this.device_id);
    this.did = item.Device_ID;
    localStorage.setItem('devices_id', item);
    this.isdeviceTripreport = localStorage.getItem('devices_id');
  }

  getTripReport() {
    this.TripReportData = [];
    if (this.datetimeEnd <= this.datetimeStart && this.device_id) {
      let alert = this.alertCtrl.create({
        message: 'To time is always greater than From Time',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.apicalligi.startLoading().present();
      this.apicalligi.trip_detailCall(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.device_id)
        .subscribe(data => {
          this.apicalligi.stopLoading();
          this.TripsdataAddress = [];
          if (data.length > 0) {
            this.tripFunction(data)
          } else {
            let toast = this.toastCtrl.create({
              message: "Report(s) not found for selected dates/Vehicles.",
              duration: 1500,
              position: "bottom"
            })
            toast.present();
          }
        },
          err => {
            this.apicalligi.stopLoading();
            console.log(err);
          });
    }
  }

  tripFunction(data) {
    let that = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      var deviceId = data[i]._id;
      var distanceBt = data[i].distance / 1000;

      var gmtDateTime = moment.utc(JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate = moment.utc(JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Startetime = gmtDateTime.local().format(' h:mm a');
      var Startdate = gmtDate.format('ll');
      var gmtDateTime1 = moment.utc(JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate1 = moment.utc(JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Endtime = gmtDateTime1.local().format(' h:mm a');
      var Enddate = gmtDate1.format('ll');

      var startDate = new Date(data[i].start_time).toLocaleString();
      var endDate = new Date(data[i].end_time).toLocaleString();

      var fd = new Date(startDate).getTime();
      var td = new Date(endDate).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
      that.TripReportData.push(
        {
          'Device_Name': data[i].device.Device_Name,
          'Device_ID': data[i].device.Device_ID,
          'Startetime': Startetime,
          'Startdate': Startdate,
          'Endtime': Endtime,
          'Enddate': Enddate,
          'distance': distanceBt,
          '_id': deviceId,
          'start_time': data[i].start_time,
          'end_time': data[i].end_time,
          'duration': Durations,
          'end_location': {
            'lat': data[i].end_lat,
            'long': data[i].end_long
          },
          'start_location': {
            'lat': data[i].start_lat,
            'long': data[i].start_long
          }
        });

      that.start_address(that.TripReportData[i], i);
      that.end_address(that.TripReportData[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }

  start_address(item, index) {
    let that = this;
    that.TripReportData[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.TripReportData[index].StartLocation = "N/A";
    } else if (item.start_location) {
      this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
        .then((res) => {
          console.log("test" ,res)
          var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
          that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
          that.TripReportData[index].StartLocation = str;
        })
    }
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalligi.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

  end_address(item, index) {
    let that = this;
    that.TripReportData[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.TripReportData[index].EndLocation = "N/A";
    } else if (item.end_location) {
      this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
      .then((res) => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
        that.TripReportData[index].EndLocation = str;
      })
    }
  }

  tripReview(tripData) {
    this.navCtrl.push('TripReviewPage', {
      params: tripData,
      device_id: this.did
    })
  }

}
